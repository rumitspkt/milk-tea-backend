import AccountModel from '../models/AccountModel';
import CategoryModel from '../models/CategoryModel';
import NotificationModel from '../models/NotificationModel';
import OrderModel from '../models/OrderModel';
import { Pagination, KeyConfig, DefaultValue } from '../commons/Types';
import AdminHandler from '../handlers/AdminHandler';

class UserHandler {
    async getAccount(userId) {
        const user = await AccountModel.findOne({ _id: userId });
        return user;
    }

    async updateProfile(userId, data) {
        const {
            password,
            phone,
            address,
            email
        } = data;
        const user = await AccountModel.findById(userId);
        if (!user) return null;
        if (password) {
            user.set({ password });
        }
        if (phone) {
            user.set({ phone });
        }
        if (address) {
            user.set({ address });
        }
        if (email) {
            user.set({ email });
        }
        const userUpdate = await user.save();
        return userUpdate;
    }

    async getCategories(page, pageSize) {
        const options = {
            page: parseInt(page) || 1,
            limit: parseInt(pageSize) || Pagination.perPageLimit
        };
        const categories = await CategoryModel.paginate(
            {},
            options,
        );
        return categories;
    }

    async getNotifications(page, pageSize) {
        const options = {
            page: parseInt(page) || 1,
            limit: parseInt(pageSize) || Pagination.perPageLimit
        };
        const notifications = await NotificationModel.paginate(
            {},
            options,
        );
        return notifications;
    }

    createOrder(data) {
        return OrderModel.create(data);
    }

    async caculatePrice(drinks, promotionId, lat, long) {
        const config = await AdminHandler.getConfig(KeyConfig.FeeShip);
        const feeShipPerKm = config ? config.value : DefaultValue.FeeShip;
        // todo: map api
        let totalDistance = 10;
        let feeShip = 10 * feeShipPerKm;
        let totalPrice = 0;
        await this.asyncForEach(drinks, async drink => {
            let price = 0;
            const mainDrink = await AdminHandler.getMainDrink(drink.category_id, drink.main_drink_id);
            if (mainDrink) {
                await this.asyncForEach(drink.toppings, async toppingId => {
                    const topping = await AdminHandler.getToppingById(toppingId);
                    price += topping.price;
                });
                price += mainDrink.price;
                totalPrice += (price * (drink.quantity ? drink.quantity : 1));
            }
        });
        let promotionValue = 0;
        if (promotionId) {
            const promotion = await AdminHandler.getPromotionById(promotionId);
            promotionValue = promotion.value;
        }
        const finalPrice = totalPrice + feeShipPerKm * totalDistance - promotionValue <= 0 ? 0 : totalPrice + feeShipPerKm * totalDistance - promotionValue;
        return {
            feeShipPerKm,
            totalDistance,
            totalPrice,
            promotionValue,
            totalFeeShip: feeShipPerKm * totalDistance,
            finalPrice
        }
    }

    async asyncForEach(array, callback) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }

}

export default new UserHandler();
