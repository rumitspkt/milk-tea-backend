export const ExampleType = {
  PROPS1: 'PROPS1',
  PROPS2: 'PROPS2',
  PROPS3: 'PROPS3',
};

// product status
export const ProductStatus = {
  IN_STOCK: 'IN_STOCK',
  OUT_STOCK: 'OUT_STOCK',
};

export const Pagination = {
  perPageLimit: 20,
};

export const Role = {
  Admin: 'admin',
  User: 'user',
  Staff: 'staff'
}

export const OrderState = {
  Ordered: 'ordered',
  Confirmed: 'confirmed',
  ToDelivery: 'to_delivery',
  Completed: 'completed',
  Canceled: 'canceled'
}

export const KeyConfig = {
  StoreLocation: 'store_location',
  FeeShip: 'fee_ship'
}

export const DefaultValue = {
  FeeShip: 5000
}
