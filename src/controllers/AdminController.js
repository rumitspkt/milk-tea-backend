import ErrorCodes from "../commons/ErrorCodes";
import AlreadyError from "../errors/already-exists";
import { Pagination } from "../commons/Types";
import ValidateError from "../errors/validation";
import AdminHandler from "../handlers/AdminHandler";
import NotImplementedError from "../errors/not-implemented";
import NotFoundError from "../errors/not-found";
import ForbiddenError from "../errors/forbidden";
import UnauthorizedError from "../errors/unauthorized";
import ToppingModel from "../models/ToppingModel";
import SocketInstance from "../utils/socket";
import UserHandler from "../handlers/UserHandler";

class AdminController {
  async createAccount(req, res) {
    const { username, role, password, phone, email, address } = req.body;
    try {
      if (!username) {
        throw new ValidateError(ErrorCodes.USERNAME_IS_EMPTY);
      }
      if (!password) {
        throw new ValidateError(ErrorCodes.PASSWORD_IS_EMPTY);
      }
      if (!role) {
        throw new ValidateError(ErrorCodes.ROLE_IS_EMPTY);
      }
      const user = await AdminHandler.getUserByUsername(username);
      if (user) {
        throw new AlreadyError(ErrorCodes.ALREADY_HAVE_THIS_USER);
      }
      const newUser = await AdminHandler.createNewUser({
        username,
        password,
        role,
        phone,
        email,
        address
      });
      res.onSuccess("CREATE_ACCOUNT_SUCCESS");
    } catch (error) {
      res.onError(error);
    }
  }

  async updateAccount(req, res) {
    const {
      userId,
      password,
      role,
      phone,
      address,
      email,
      last_login
    } = req.body;
    try {
      if (!userId) {
        throw new ValidateError(ErrorCodes.USER_ID_IS_EMPTY);
      }
      const userUpdate = await AdminHandler.updateUser(userId, {
        password,
        role,
        phone,
        address,
        email,
        last_login
      });
      if (userUpdate) {
        res.onSuccess("UPDATE_ACCOUNT_SUCCESS");
      } else {
        throw new NotImplementedError("UPDATE_ACCOUNT_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async deleteAccount(req, res) {
    const { userId } = req.body;
    try {
      if (!userId) {
        throw new ValidateError(ErrorCodes.USER_ID_IS_EMPTY);
      }
      const isDeleted = await AdminHandler.deleteAccount(userId);
      if (isDeleted) {
        res.onSuccess("DELETE_ACCOUNT_SUCCESS");
      } else {
        throw new NotImplementedError("DELETE_ACCOUNT_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async getAccounts(req, res) {
    const { page, pageSize } = req.query;
    try {
      if (pageSize > Pagination.perPageLimit) {
        throw new ValidateError(ErrorCodes.PAGE_SIZE_INVALID);
      }
      const accounts = await AdminHandler.getAccounts(page, pageSize);
      res.onSuccess(accounts);
    } catch (error) {
      res.onError(error);
    }
  }

  async getAccount(req, res) {
    const { userId } = req.params;
    try {
      if (!userId) {
        throw new ValidateError(ErrorCodes.USER_ID_IS_EMPTY);
      }
      const user = await AdminHandler.getAccount(userId);
      if (!user) {
        throw new NotFoundError(ErrorCodes.NOT_FOUND);
      }
      res.onSuccess({
        username: user.username,
        role: user.role,
        address: user.address,
        email: user.email,
        last_login: user.last_login,
        cart: user.cart,
        phone: user.phone
      });
    } catch (error) {
      res.onError(error);
    }
  }

  async createCategory(req, res) {
    const { name, code, image_url } = req.body;
    try {
      if (!name) {
        throw new ValidateError(ErrorCodes.NAME_IS_EMPTY);
      }
      if (!code) {
        throw new ValidateError(ErrorCodes.CODE_IS_EMPTY);
      }
      const category = await AdminHandler.getCategory(code);
      if (category) {
        throw new AlreadyError(ErrorCodes.CODE_IS_EXIST);
      }
      const newCategory = await AdminHandler.createNewCategory({
        name,
        code,
        image_url
      });
      res.onSuccess("CREATE_CATEGORY_SUCCESS");
    } catch (error) {
      res.onError(error);
    }
  }

  async updateCategory(req, res) {
    const { categoryId, name, image_url } = req.body;
    try {
      if (!categoryId) {
        throw new ValidateError(ErrorCodes.CATEGORY_ID_IS_EMPTY);
      }
      const category = await AdminHandler.getCategoryById(categoryId);
      if (!category) {
        throw new NotFoundError(ErrorCodes.NOT_FOUND);
      }
      const categoryUpdate = await AdminHandler.updateCategory(categoryId, {
        name,
        image_url
      });
      if (categoryUpdate) {
        res.onSuccess("UPDATE_CATEGORY_SUCCESS");
      } else {
        throw new NotImplementedError("UPDATE_CATEGORY_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async deleteCategory(req, res) {
    const { categoryId } = req.body;
    try {
      if (!categoryId) {
        throw new ValidateError(ErrorCodes.CATEGORY_ID_IS_EMPTY);
      }
      const isDeleted = await AdminHandler.deleteCategory(categoryId);
      if (isDeleted) {
        res.onSuccess("DELETE_CATEGORY_SUCCESS");
      } else {
        throw new NotImplementedError("DELETE_CATEGORY_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async getCategories(req, res) {
    const { page, pageSize } = req.query;
    try {
      if (pageSize > Pagination.perPageLimit) {
        throw new ValidateError(ErrorCodes.PAGE_SIZE_INVALID);
      }
      const categories = await AdminHandler.getCategories(page, pageSize);
      res.onSuccess(categories);
    } catch (error) {
      res.onError(error);
    }
  }

  async getCategory(req, res) {
    const { categoryId } = req.params;
    try {
      if (!categoryId) {
        throw new ValidateError(ErrorCodes.CATEGORY_ID_IS_EMPTY);
      }
      const category = await AdminHandler.getCategoryById(categoryId);
      if (!category) {
        throw new NotFoundError(ErrorCodes.NOT_FOUND);
      }
      res.onSuccess(category);
    } catch (error) {
      res.onError(error);
    }
  }

  // main drink

  async createMainDrink(req, res) {
    const { categoryId, name, price, rating, image_url } = req.body;
    try {
      if (!categoryId) {
        throw new ValidateError(ErrorCodes.CATEGORY_ID_IS_EMPTY);
      }
      if (!name) {
        throw new ValidateError(ErrorCodes.NAME_IS_EMPTY);
      }
      if (!price) {
        throw new ValidateError(ErrorCodes.PRICE_IS_EMPTY);
      }
      const category = await AdminHandler.getCategoryById(categoryId);
      if (!category) {
        throw new NotFoundError(ErrorCodes.NOT_FOUND);
      }
      const isUpdated = await AdminHandler.createMainDrink(categoryId, {
        name,
        price,
        rating,
        image_url
      });
      if (isUpdated) {
        res.onSuccess("CREATE_MAIN_DRINK_SUCCESS");
      } else {
        throw new NotImplementedError("CREATE_MAIN_DRINK_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async updateMainDrinks(req, res) {
    const {
      categoryId,
      mainDrinkId,
      name,
      price,
      rating,
      image_url
    } = req.body;
    try {
      if (!categoryId) {
        throw new ValidateError(ErrorCodes.CATEGORY_ID_IS_EMPTY);
      }
      if (!mainDrinkId) {
        throw new ValidateError(ErrorCodes.MAIN_DRINK_ID_IS_EMPTY);
      }
      if (
        await AdminHandler.updateMainDrink(categoryId, mainDrinkId, {
          name,
          price,
          rating,
          image_url
        })
      ) {
        res.onSuccess("UPDATE_MAIN_DRINK_SUCCESS");
      } else {
        throw new NotImplementedError("UPDATE_MAIN_DRINK_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async getMainDrinks(req, res) {
    const { categoryId } = req.params;
    try {
      if (!categoryId) {
        throw new ValidateError(ErrorCodes.CATEGORY_ID_IS_EMPTY);
      }
      const category = await AdminHandler.getCategoryById(categoryId);
      if (!category) {
        throw new NotFoundError(ErrorCodes.NOT_FOUND);
      }
      res.onSuccess(category.main_drinks);
    } catch (error) {
      res.onError(error);
    }
  }

  async deleteMainDrink(req, res) {
    const { categoryId, mainDrinkId } = req.body;
    try {
      if (!categoryId) {
        throw new ValidateError(ErrorCodes.CATEGORY_ID_IS_EMPTY);
      }
      if (!mainDrinkId) {
        throw new ValidateError(ErrorCodes.MAIN_DRINK_ID_IS_EMPTY);
      }
      if (await AdminHandler.deleteMainDrink(categoryId, mainDrinkId)) {
        res.onSuccess("DELETE_MAIN_DRINK_SUCCESS");
      } else {
        throw new NotImplementedError("DELETE_MAIN_DRINK_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async createTopping(req, res) {
    const { code, name, image_url, price } = req.body;
    try {
      if (!code) {
        throw new ValidateError(ErrorCodes.CODE_IS_EMPTY);
      }
      if (!name) {
        throw new ValidateError(ErrorCodes.NAME_IS_EMPTY);
      }
      if (!price) {
        throw new ValidateError(ErrorCodes.PRICE_IS_EMPTY);
      }
      const topping = await AdminHandler.getTopping(code);
      if (topping) {
        throw new AlreadyError(ErrorCodes.CODE_IS_EXIST);
      }
      await AdminHandler.createNewTopping({
        code,
        name,
        image_url,
        price
      });
      res.onSuccess("CREATE_TOPPING_SUCCESS");
    } catch (error) {
      res.onError(error);
    }
  }

  async updateTopping(req, res) {
    const { toppingId, name, image_url, price } = req.body;
    try {
      if (!toppingId) {
        throw new ValidateError(ErrorCodes.TOPPING_ID_IS_EMPTY);
      }
      const topping = await AdminHandler.getToppingById(toppingId);
      if (!topping) {
        throw new NotFoundError(ErrorCodes.NOT_FOUND);
      }
      const toppingUpdate = await AdminHandler.updateTopping(toppingId, {
        name,
        image_url,
        price
      });
      if (toppingUpdate) {
        res.onSuccess("UPDATE_TOPPING_SUCCESS");
      } else {
        throw new NotImplementedError("UPDATE_TOPPING_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async deleteTopping(req, res) {
    const { toppingId } = req.body;
    try {
      if (!toppingId) {
        throw new ValidateError(ErrorCodes.TOPPING_ID_IS_EMPTY);
      }
      const isDeleted = await AdminHandler.deleteTopping(toppingId);
      if (isDeleted) {
        res.onSuccess("DELETE_TOPPING_SUCCESS");
      } else {
        throw new NotImplementedError("DELETE_TOPPING_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async getToppings(req, res) {
    const { page, pageSize } = req.query;
    try {
      if (pageSize > Pagination.perPageLimit) {
        throw new ValidateError(ErrorCodes.PAGE_SIZE_INVALID);
      }
      const toppings = await AdminHandler.getToppings(page, pageSize);
      res.onSuccess(toppings);
    } catch (error) {
      res.onError(error);
    }
  }

  async createNotification(req, res) {
    const { title, content, image_url, time_start, time_end } = req.body;
    try {
      if (!title) {
        throw new ValidateError(ErrorCodes.TITLE_IS_EMPTY);
      }
      await AdminHandler.createNotification({
        title,
        content,
        image_url,
        time_start,
        time_end
      });
      res.onSuccess("CREATE_NOTIFICATION_SUCCESS");
    } catch (error) {
      res.onError(error);
    }
  }

  async updateNotification(req, res) {
    const {
      notificationId,
      title,
      content,
      image_url,
      time_start,
      time_end
    } = req.body;
    try {
      if (!notificationId) {
        throw new ValidateError(ErrorCodes.NOTIFICATION_ID_IS_EMPTY);
      }
      const notification = await AdminHandler.getNotificationById(notificationId);
      if (!notification) {
        throw new NotFoundError(ErrorCodes.NOT_FOUND);
      }
      const notificationUpdate = await AdminHandler.updateNotification(
        notificationId,
        {
          title,
          content,
          image_url,
          time_start,
          time_end
        }
      );
      if (notificationUpdate) {
        res.onSuccess("UPDATE_NOTIFICATION_SUCCESS");
      } else {
        throw new NotImplementedError("UPDATE_NOTIFICATION_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async deleteNotification(req, res) {
    const { notificationId } = req.body;
    try {
      if (!notificationId) {
        throw new ValidateError(ErrorCodes.NOTIFICATION_ID_IS_EMPTY);
      }
      const isDeleted = await AdminHandler.deleteNotification(notificationId);
      if (isDeleted) {
        res.onSuccess("DELETE_NOTIFICATION_SUCCESS");
      } else {
        throw new NotImplementedError("DELETE_NOTIFICATION_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async getNotifications(req, res) {
    const { page, pageSize } = req.query;
    try {
      if (pageSize > Pagination.perPageLimit) {
        throw new ValidateError(ErrorCodes.PAGE_SIZE_INVALID);
      }
      const toppings = await AdminHandler.getNotifications(page, pageSize);
      res.onSuccess(toppings);
    } catch (error) {
      res.onError(error);
    }
  }

  async createPromotion(req, res) {
    const { code, title, content, image_url, value, time_start, time_end } = req.body;
    try {
      if (!code) {
        throw new ValidateError(ErrorCodes.CODE_IS_EMPTY);
      }
      const promotion = await AdminHandler.getPromotion(code);
      if (promotion) {
        throw new AlreadyError(ErrorCodes.CODE_IS_EXIST);
      }
      await AdminHandler.createPromotion({
        code,
        title,
        content,
        image_url,
        value,
        time_start,
        time_end
      });
      res.onSuccess("CREATE_PROMOTION_SUCCESS");
    } catch (error) {
      res.onError(error);
    }
  }

  async updatePromotion(req, res) {
    const {
      promotionId,
      title,
      content,
      image_url,
      value,
      time_start,
      time_end
    } = req.body;
    try {
      if (!promotionId) {
        throw new ValidateError(ErrorCodes.PROMOTION_ID_IS_EMPTY);
      }
      const promotion = await AdminHandler.getPromotionById(promotionId);
      if (!promotion) {
        throw new NotFoundError(ErrorCodes.NOT_FOUND);
      }
      const promotionUpdate = await AdminHandler.updatePromotion(
        promotionId,
        {
          title,
          content,
          value,
          image_url,
          time_start,
          time_end
        }
      );
      if (promotionUpdate) {
        res.onSuccess("UPDATE_PROMOTION_SUCCESS");
      } else {
        throw new NotImplementedError("UPDATE_PROMOTION_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async deletePromotion(req, res) {
    const { promotionId } = req.body;
    try {
      if (!promotionId) {
        throw new ValidateError(ErrorCodes.PROMOTION_ID_IS_EMPTY);
      }
      const isDeleted = await AdminHandler.deletePromotion(promotionId);
      if (isDeleted) {
        res.onSuccess("DELETE_PROMOTION_SUCCESS");
      } else {
        throw new NotImplementedError("DELETE_PROMOTION_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async getPromotions(req, res) {
    const { page, pageSize } = req.query;
    try {
      if (pageSize > Pagination.perPageLimit) {
        throw new ValidateError(ErrorCodes.PAGE_SIZE_INVALID);
      }
      const promotions = await AdminHandler.getPromotions(page, pageSize);
      res.onSuccess(promotions);
    } catch (error) {
      res.onError(error);
    }
  }

  async createConfig(req, res) {
    const { key, value, description } = req.body;
    try {
      if (!key) {
        throw new ValidateError(ErrorCodes.KEY_IS_EMPTY);
      }
      const config = await AdminHandler.getConfig(key);
      if (config) {
        throw new AlreadyError(ErrorCodes.KEY_IS_EXIST);
      }
      await AdminHandler.createConfig({
        key,
        value,
        description
      });
      res.onSuccess("CREATE_CONFIG_SUCCESS");
    } catch (error) {
      res.onError(error);
    }
  }

  async updateConfig(req, res) {
    const {
      key,
      value,
      description
    } = req.body;
    try {
      if (!key) {
        throw new ValidateError(ErrorCodes.KEY_IS_EMPTY);
      }
      const config = await AdminHandler.getConfig(key);
      if (!config) {
        throw new NotFoundError(ErrorCodes.NOT_FOUND);
      }
      const configUpdate = await AdminHandler.updateConfig(
        key,
        {
          value,
          description
        }
      );
      if (configUpdate) {
        res.onSuccess("UPDATE_CONFIG_SUCCESS");
      } else {
        throw new NotImplementedError("UPDATE_CONFIG_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async deleteConfig(req, res) {
    const { key } = req.body;
    try {
      if (!key) {
        throw new ValidateError(ErrorCodes.KEY_IS_EMPTY);
      }
      const isDeleted = await AdminHandler.deleteConfig(key);
      if (isDeleted) {
        res.onSuccess("DELETE_CONFIG_SUCCESS");
      } else {
        throw new NotImplementedError("DELETE_CONFIG_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async getConfigs(req, res) {
    const { page, pageSize } = req.query;
    try {
      if (pageSize > Pagination.perPageLimit) {
        throw new ValidateError(ErrorCodes.PAGE_SIZE_INVALID);
      }
      const configs = await AdminHandler.getConfigs(page, pageSize);
      res.onSuccess(configs);
    } catch (error) {
      res.onError(error);
    }
  }

  async updateOrder(req, res) {
    const {
      userId,
      orderId,
      shipper_id,
      status,
      address,
      lat_addr,
      long_addr,
      promotion_id,
      note,
      drinks
    } = req.body;
    try {
      if (!orderId) {
        throw new ValidateError(ErrorCodes.ORDER_ID_IS_EMPTY);
      }
      const order = await AdminHandler.getOrder(orderId);
      if (!order) {
        throw new NotFoundError(ErrorCodes.NOT_FOUND);
      }
      const orderUpdate = await AdminHandler.updateOrder(
        orderId,
        {
          shipper_id,
          status,
          address,
          lat_addr,
          long_addr,
          promotion_id,
          note,
          drinks
        }
      );
      if (orderUpdate) {
        if (status) {
          SocketInstance.io.to('user_room_' + userId).emit('new_state', {
            user_id: userId,
            order_id: orderId,
            new_state: status
          });
        }
        res.onSuccess("UPDATE_ORDER_SUCCESS");
      } else {
        throw new NotImplementedError("UPDATE_ORDER_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async deleteOrder(req, res) {
    const { orderId } = req.body;
    try {
      if (!orderId) {
        throw new ValidateError(ErrorCodes.ORDER_ID_IS_EMPTY);
      }
      const isDeleted = await AdminHandler.deleteOrder(orderId);
      if (isDeleted) {
        res.onSuccess("DELETE_ORDER_SUCCESS");
      } else {
        throw new NotImplementedError("DELETE_ORDER_ERROR");
      }
    } catch (error) {
      res.onError(error);
    }
  }

  async getOrders(req, res) {
    const { page, pageSize } = req.query;
    try {
      if (pageSize > Pagination.perPageLimit) {
        throw new ValidateError(ErrorCodes.PAGE_SIZE_INVALID);
      }
      const orders = await AdminHandler.getOrders(page, pageSize);
      res.onSuccess(orders);
    } catch (error) {
      res.onError(error);
    }
  }

  async getOrdersOfUser(req, res) {
    const { userId } = req.params;
    const { page, pageSize } = req.query;
    try {
      if (pageSize > Pagination.perPageLimit) {
        throw new ValidateError(ErrorCodes.PAGE_SIZE_INVALID);
      }
      const orders = await AdminHandler.getOrdersOfUser(page, pageSize, userId);
      res.onSuccess(orders);
    } catch (error) {
      res.onError(error);
    }
  }

  async getOrderDetail(req, res) {
    const { orderId } = req.params;
    try {
      const order = await AdminHandler.getOrderDetail(orderId);
      res.onSuccess(order);
    } catch (error) {
      res.onError(error);
    }
  }
}

export default new AdminController();
