import UserController from '../../controllers/UserController';
import AdminController from '../../controllers/AdminController';
import RoleMiddleware from '../../middlewares/RoleMiddleware';

const router = require('express').Router();

// profile
router.use('/profile', RoleMiddleware.roleUser);

router.put('/profile', UserController.updateProfile);
router.get('/profile', UserController.getProfile);

// category
router.get('/category', UserController.getCategories);

// topping
router.get('/topping', AdminController.getToppings);

// notifcation 
router.use('/notification', RoleMiddleware.roleUser);

router.get('/notification', UserController.getNotifications);

// order
router.use('/order', RoleMiddleware.roleUser);

router.post('/order', UserController.createOrder);
router.put('/order', UserController.updateOrder);
router.get('/order', UserController.getOrders);
router.post('/order/calculate_price', UserController.caculatePrice);

// promotion
router.use('/promotion', RoleMiddleware.roleUser);
router.get('/promotion', AdminController.getPromotions);

export default router;
