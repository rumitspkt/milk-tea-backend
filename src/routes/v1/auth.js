import AuthController from '../../controllers/AuthController';
const passport = require('passport')

const router = require('express').Router();

router.post('/login', AuthController.login);
router.post('/register', AuthController.register);
router.get('/login/google', AuthController.getGoogleLoginUrl);
router.get('/google/callback', AuthController.loginGoogle);
router.get('/login/facebook', passport.authenticate('facebook', { scope: 'email' }));
router.get('/facebook/callback', passport.authenticate('facebook'), AuthController.loginFacebook);

export default router;
