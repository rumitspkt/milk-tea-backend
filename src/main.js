import http from 'http';
import BootMongo from './boots/BootMongo';
import BootExpress from './boots/BootExpress';
import { PORT } from './config/Server';
import SocketInstance from './utils/socket'
import { Role } from './commons/Types';
import { userInfo } from 'os';
import { verifyToken } from './utils/jwt';

const runApp = async () => {
  try {
    await BootMongo();
    // Run services

    const app = BootExpress();

    const server = http.createServer(app);

    const io = require('socket.io')(server);

    io.on('connection', function (socket) {
      console.log('connected socket ', socket.id);
      socket.auth = false;

      socket.on('authenticate', function (data) {
        let payload;
        try {
          payload = verifyToken(data.token);
          console.log("Authenticated socket ", socket.id);
        } catch (error) {
          payload = null;
          console.log("Error authenticate socket ", socket.id);
        }
        if (payload) {
          socket.auth = true;
          if (payload.role == Role.User) {
            socket.join('user_room' + payload.userId);
            console.log('socket joined ', socket.id, 'user_room_' + payload.userId);
            let nspSockets = io.in('user_room_' + payload.userId).sockets;
            console.log('number socket in room', Object.keys(nspSockets).length);
          } else if (payload.role == Role.Staff) {
            socket.join('staff_room');
            console.log('socket joined ', socket.id, 'staff_room');
            let nspSockets = io.in('staff_room').sockets;
            console.log('number socket in room', Object.keys(nspSockets).length);
          }
        }
        console.log('=====================');
      });

      setTimeout(function () {
        if (!socket.auth) {
          console.log("disconnecting socket ", socket.id);
          socket.disconnect('unauthorized');
          console.log('=====================');
        }
      }, 5000);

      socket.on('disconnect', function () {
        console.log('disconnected socket');
      });

    });

    SocketInstance.io = io;

    server.listen(process.env.PORT || PORT, () => console.log(`Listening on port ${process.env.PORT || PORT}`));
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
};

runApp();
