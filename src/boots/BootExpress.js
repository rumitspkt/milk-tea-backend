import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import cors from 'cors';
import cookieParser from 'cookie-parser';
// import routes
import routes from '../routes';
// import response
import responseHelper from '../helpers/ResponseHelper';

const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;

// Passport session setup.
passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (obj, done) {
  done(null, obj);
});

// Use the FacebookStrategy within Passport.
passport.use(new FacebookStrategy({
  clientID: process.env.FB_CLIENT_ID || '2549347291964106',
  clientSecret: process.env.FB_CLIENT_SECRET || '46d284397be4d3cc1df2ed8c93644c0b',
  callbackURL: process.env.FB_CALLBACK_URL || 'http://localhost:9000/v1/auth/facebook/callback'
},
  function (accessToken, refreshToken, profile, done) {
    process.nextTick(function () {
      console.log(accessToken, refreshToken, profile, done);
      return done(null, profile);
    });
  }
));

export default () => {
  const app = express();

  app.disable('x-powered-by');
  // logger
  app.use(morgan('dev'));

  app.use(cookieParser());

  app.use(bodyParser.json());

  app.use(cors());

  app.use(responseHelper.middlewareResponse);

  app.use(passport.initialize());

  app.use(passport.session());

  app.use('/', routes);

  return app;
};
