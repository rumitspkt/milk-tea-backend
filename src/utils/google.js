import { google } from 'googleapis';

const googleConfig = {
    clientId: process.env.GG_CLIENT_ID || '47252580457-4h8m2mrvoo64o2jmt72e88tk9m2uid2e.apps.googleusercontent.com',
    clientSecret: process.env.GG_CLIENT_SECRET || 'wJJtwPUrnhd1gIsRpRUlZxhz',
    redirect: process.env.GG_REDIRECT || 'http://localhost:9000/v1/auth/google/callback',
};

const defaultScope = [
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/userinfo.profile',
];

var auth = createConnection();

function createConnection() {
    return new google.auth.OAuth2(
        googleConfig.clientId,
        googleConfig.clientSecret,
        googleConfig.redirect
    );
}

function getConnectionUrl(auth) {
    return auth.generateAuthUrl({
        access_type: 'offline',
        scope: defaultScope.join(' ')
    });
}

export const urlGoogle = () => {
    const url = getConnectionUrl(auth);
    return url;
}

export const getGoogleAccountFromCode = async (code) => {
    const data = await auth.getToken(code);
    const tokens = data.tokens;
    auth = createConnection();
    auth.setCredentials(tokens);
    const people = google.people({
        version: 'v1',
        auth,
    });
    const res = await people.people.get({
        resourceName: 'people/me',
        personFields: 'emailAddresses,names,photos',
    });
    return {
        name: res.data.names[0].displayName,
        avatar_url: res.data.photos[0].url,
        email: res.data.emailAddresses[0].value,
    };
}



